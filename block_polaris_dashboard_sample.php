<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Block polaris_dashboard_sample is defined here.
 *
 * @package     block_polaris_dashboard_sample
 * @copyright   2023 Digital Learning GmbH <support@digitallearning.gmbh>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_polaris_dashboard_sample extends block_base {

    /**
     * Initializes class member variables.
     */
    public function init() {
        // Needed by Moodle to differentiate between blocks.
      
        $this->title = get_string('pluginname', 'block_polaris_dashboard_sample');
    }
    
	public function get_content() {

		if ($this->content !== null) {
		  return $this->content;
		}
		
		$jslink = new moodle_url('/blocks/polaris_dashboard_sample/bundle.js');

		$this->content         =  new stdClass;
		$this->content->text   = '<div class="wrapper"></div><div class="grid"><div class="grid-stack"></div></div><div class="modal fade" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-body" id="modal-content-body"></div></div></div></div><div class="modal fade" id="myErrorModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-body" id="error-modal-content-body"></div></div></div></div><script defer="defer" src="'.$jslink.'"></script>';
		$this->content->footer = 'Footer here...';
	 
		return $this->content;
	}

}
