import {GroupedBarChart} from "./js/custom-charts/grouped_bar_chart";
import * as d3 from "d3";

const dict = {
    "2023-04-28": {
        "accessed": 98,
        "started": 7,
        "graded": 7,
        "submitted": 7,
        "answered": 38,
        "leftUnanswered": 4
    },
    "2023-04-29": {
        "accessed": 2
    },
    "2023-04-30": {
        "accessed": 1
    },
    "2023-05-01": {
        "accessed": 1
    },
    "2023-05-02": {
        "accessed": 45,
        "started": 4,
        "submitted": 4,
        "answered": 16,
        "leftUnanswered": 8,
        "graded": 2
    }
}

const data = Object.keys(dict).reduce( (agg, key) => [...agg, {...dict[key], name : key}], [])
const categories = Object.keys(data[0])
const names = data.reduce( (agg, curr) => [...agg, curr["name"]], [])
const dataFlatted = categories.flatMap(category => data.map(d => ({name : d.name, category : category, value: d[category]}))) // pivot longer


export const groupedBarChartDef = { "grouped-barchart-widget" :
    new GroupedBarChart(
        "A sample grouped bar chart",
        "sample description",
        dataFlatted,
        {
            x: d => d.name,
            y: d => d.value,
            z: d => d.category,

            xDomain: names,
            //yDomain : [0,40],
            zDomain: categories,
            colors: d3.schemeSpectral[categories.length]
        })
}