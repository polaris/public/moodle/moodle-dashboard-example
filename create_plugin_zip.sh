#!/bin/bash

DIR_NAME=polaris_dashboard_plugin

mkdir $DIR_NAME

cp version.php $DIR_NAME
cp js_plugin/dist/bundle.js $DIR_NAME
cp block_polaris_dashboard_sample.php $DIR_NAME
cp -r db $DIR_NAME
cp -r lang $DIR_NAME

zip -r $DIR_NAME.zip $DIR_NAME
rm -rf $DIR_NAME
